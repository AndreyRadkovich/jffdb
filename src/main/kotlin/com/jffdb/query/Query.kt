package com.jffdb.query

interface Query {
  fun exec(): Unit
}